package gr.petalidis.musicinfo.ui;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import gr.petalidis.musicinfo.AssetManagerReader;
import gr.petalidis.musicinfo.R;
import gr.petalidis.musicinfo.audiodbclient.ArtistInfoClient;
import gr.petalidis.musicinfo.audiodbclient.ArtistInfoRequest;
import gr.petalidis.musicinfo.dto.ArtistDto;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class ArtistInfoUITest {

    @Rule
    public ActivityScenarioRule<ArtistInfoUI> activityRule
            = new ActivityScenarioRule<>(ArtistInfoUI.class);
    NetworkResponse networkResponse;
    @Before
    public void setUp() throws Exception
    {
        String response = AssetManagerReader.readFile(InstrumentationRegistry.getInstrumentation().getContext(),"princeAndroidTest.json");
        networkResponse = new NetworkResponse(response.getBytes());
    }
    @Test
    public void checkThatWhenArtistIsPrinceWeGetBackTheCorrectScreenWithMock() {
        Answer<Void> returnSuccess = invocation -> {
            Object argument = invocation.getArgument(0);
            ArtistInfoRequest artistInfoRequest = (ArtistInfoRequest)argument;
            Response<ArtistDto> artistDtoResponse = artistInfoRequest.parseResponse(networkResponse);
            artistInfoRequest.deliver(artistDtoResponse);
            return null;
        };

        ArtistInfoClient client = Mockito.mock(ArtistInfoClient.class);

        ArtistInfoClient.setClient(client);

        doAnswer(returnSuccess)
                .when(client).buildAndSendRequest(any(ArtistInfoRequest.class));

        // Type text and then press the button.
        onView(withId(R.id.searchArtistTextView))
                .perform(typeText("Prince"), closeSoftKeyboard());
        onView(withId(R.id.searchButton)).perform(click());

        // Check that the text was changed.
        onView(withId(R.id.artistNameTextView))
                .check(matches(withText("Prince")));

    }
//    @Test
//    public void checkThatWhenArtistIsPrinceWeGetBackTheCorrectScreenWithoutMock() {
//        // Type text and then press the button.
//        onView(withId(R.id.searchArtistTextView))
//                .perform(typeText("prince"), closeSoftKeyboard());
//        onView(withId(R.id.searchButton)).perform(click());
//
//        // Check that the text was changed.
//        onView(withId(R.id.artistNameTextView))
//                .check(matches(withText(equalToIgnoringCase("prince"))));
//
//    }
}
