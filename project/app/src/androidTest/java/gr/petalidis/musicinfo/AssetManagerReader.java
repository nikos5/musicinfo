package gr.petalidis.musicinfo;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AssetManagerReader {

    public static String readFile(Context appContext, String filename) throws Exception
    {
        StringBuilder contents = new StringBuilder();
        try (InputStream in = appContext.getAssets().open(filename);
             BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
            String line;
            while ((line = br.readLine()) != null) {
                contents.append(line);
            }
            return contents.toString();
        } catch (IOException e) {
            throw  new Exception(e.getMessage());
        }
    }
}
