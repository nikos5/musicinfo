package gr.petalidis.musicinfo.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

//import gr.petalidis.musicinfo.MyRequestQueue;
import gr.petalidis.musicinfo.MyRequestQueue;
import gr.petalidis.musicinfo.R;
import gr.petalidis.musicinfo.databinding.ActivityViewArtistInfoBinding;

public class ArtistInfoUI extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityViewArtistInfoBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyRequestQueue.createInstance(getApplicationContext());

        //MyRequestQueue.createInstance(getApplicationContext());
        binding = ActivityViewArtistInfoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);
      //  ArtistInfoUIModel model = new ViewModelProvider(this).get(ArtistInfoUIModel.class);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_view_artist_info);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_view_artist_info);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }
}