package gr.petalidis.musicinfo;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class MyRequestQueue {

    private static RequestQueue requestQueue;
    private static MyRequestQueue instance;

    private MyRequestQueue(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized void createInstance(Context context) {
        if (instance == null) {
            instance = new MyRequestQueue(context);
        }
    }
    public static void add(Request request) {
        requestQueue.add(request);
    }

}

