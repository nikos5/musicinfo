package gr.petalidis.musicinfo.mappers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import gr.petalidis.musicinfo.dto.ArtistDto;

public class ArtistDtoMapper {

    public static List<ArtistDto> fromJson(String json) throws Exception
    {
        List<ArtistDto> artistDtos = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(json);
        JSONArray jsonArray = jsonObject.getJSONArray("artists");
        for (int i =0; i<jsonArray.length(); i++) {
            artistDtos.add(fromJson(jsonArray.getJSONObject(i)));
        }
        return artistDtos;
    }
    public static ArtistDto fromJson(JSONObject json) throws Exception
    {
        ArtistDto artistDto = new ArtistDto();
        artistDto.setName(json.getString("strArtist"));
        artistDto.setBio(json.getString("strBiographyEN"));
        artistDto.setPhotoUrl(json.getString("strArtistFanart"));

        return artistDto;
    }
}
