package gr.petalidis.musicinfo.audiodbclient;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import java.util.List;

import gr.petalidis.musicinfo.dto.ArtistDto;
import gr.petalidis.musicinfo.mappers.ArtistDtoMapper;
//import gr.petalidis.musicinfo.mappers.ArtistDtoMapper;

public class ArtistInfoRequest extends Request<ArtistDto> {
    private final static String
            ARTIST_INFO_URL = "https://www.theaudiodb.com/api/v1/json/2/search.php?s=";

    private final Response.Listener<ArtistDto> listener;

    public ArtistInfoRequest(String searchTerm,
                             Response.Listener<ArtistDto> listener,
                             Response.ErrorListener errorListener) {
        super(Method.GET, ARTIST_INFO_URL+searchTerm, errorListener);
        this.listener = listener;
    }

    @Override
    protected void deliverResponse(ArtistDto response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<ArtistDto> parseNetworkResponse(
            NetworkResponse response) {
        try {
            String json = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            List<ArtistDto> artistDtos = ArtistDtoMapper.fromJson(json);
            return Response.success(artistDtos.get(0),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            Log.e(ArtistInfoRequest.class.getSimpleName(),"Unable to parse response");
            throw new IllegalStateException("");
        }

    }

    public Response<ArtistDto> parseResponse(NetworkResponse response)
    {
        return parseNetworkResponse(response);
    }

    public void deliver(Response<ArtistDto> response)
    {
         deliverResponse(response.result);
    }

}
