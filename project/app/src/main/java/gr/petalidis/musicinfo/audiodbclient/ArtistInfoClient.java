package gr.petalidis.musicinfo.audiodbclient;

import gr.petalidis.musicinfo.MyRequestQueue;

public class ArtistInfoClient {
    public static ArtistInfoClient infoClient = null;
    public static synchronized void  setClient(ArtistInfoClient artistInfoClient) {
        infoClient = artistInfoClient;
    }
    public static synchronized ArtistInfoClient getInfoClient()
    {
        if (infoClient == null) {
            return new ArtistInfoClient();
        } else {
            return infoClient;
        }
    }
    public void buildAndSendRequest(ArtistInfoRequest artistInfoRequest) {
        MyRequestQueue.add(artistInfoRequest);
    }
}
