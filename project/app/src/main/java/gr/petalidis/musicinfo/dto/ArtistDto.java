package gr.petalidis.musicinfo.dto;

public class ArtistDto {

    private String name;

    private String bio;

    private byte[] photo;

    private String photoUrl;

    public ArtistDto(String artistName) {
        this.name = artistName;
    }

    public ArtistDto()
    {

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
