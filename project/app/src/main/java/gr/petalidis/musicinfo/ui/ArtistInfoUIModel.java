package gr.petalidis.musicinfo.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import gr.petalidis.musicinfo.audiodbclient.ArtistInfoClient;
import gr.petalidis.musicinfo.audiodbclient.ArtistInfoRequest;
import gr.petalidis.musicinfo.dto.ArtistDto;

public class ArtistInfoUIModel extends ViewModel {
        private MutableLiveData<ArtistDto> artist;
        private MutableLiveData<String> errorMessage = new MutableLiveData<>("");

    public ArtistInfoUIModel() {
    }

    public void setArtistName(String artistName) {
            if (artist == null) {
              ArtistDto artistDto = new ArtistDto(artistName);
               artist = new MutableLiveData<ArtistDto>();
               artist.setValue(artistDto);
            }
        }

        public LiveData<ArtistDto> getArtist()
        {
            ArtistInfoRequest artistInfoRequest = new ArtistInfoRequest(
                    artist.getValue().getName(),
                    request ->
                    {
                        artist.setValue(request);
                    },
                    error ->
                    {
                        errorMessage.setValue(error.getMessage());
                    });
            ArtistInfoClient artistInfoClient =  ArtistInfoClient.getInfoClient();
            artistInfoClient.buildAndSendRequest(artistInfoRequest);
            return artist;
        }
}
